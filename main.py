import argparse

from modules.sqli.sqli import scan_sqli
from modules.xss.xss import scan_xss
from modules.csfr import find_csrf_token
from modules.find_inputs import find_inputs


def main():
    parser = argparse.ArgumentParser(description='Web vulnerabilities '
                                                 'detection')
    parser.add_argument('--xss', action="store", dest='xss', help="Option to "
                                                                "start xss "
                                                                  "scan", default=False)
    parser.add_argument('-u', action="store", dest="url", help="Url for "
                                                               "detection", required=True)
    parser.add_argument('--sqli', action="store", dest='sqli', help="Option to "
                                                    "start sqli "
                                                    "scan", default=False)
    parser.add_argument('--dirsearch', action='store', dest="dirsearch",
                        help="Option find all files on site")
    parser.add_argument('--csrf', action='store', dest='csrf', help='Option find CSRF token on site')
    parser.add_argument('--input', action='store', dest='input', help='Option find all form/input fields on site')
    args = parser.parse_args()
    url = args.url
    if args.xss == 'start':
        scan_xss(url)
    if args.sqli == 'start':
        scan_sqli(url)
    if args.csrf == 'start':
        find_csrf_token(url)
    if args.input == 'start':
        find_inputs(url)


if __name__ == '__main__':
    main()
