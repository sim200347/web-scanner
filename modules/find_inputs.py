from modules.base_funcs.get_all_forms import get_all_forms
from modules.base_funcs.get_form_details import get_form_details


def find_inputs(url: str):
    """
    Ищет все формы и поля ввода

    :param url: URL сайта
    :type url: str
    """
    forms = get_all_forms(url)
    if not forms:
        print("Not form detection")
        return
    i = 1
    for form in forms:
        print("Form#{} detection:".format(i))
        i += 1
        details = get_form_details(form)
        print("    Action form:", details['action'])
        print("    Method from:", details['method'])
        print("    Form have {} inputs".format(len(details['inputs'])))
        j = 1
        for entry in details['inputs']:
            print("      Input#{} detection:".format(j))
            j += 1
            print("          Type input:", entry['type'])
            print("          Name input:", entry['name'])
        print()
