"""
CSRF токен
"""
import requests
from bs4 import BeautifulSoup as bs


def find_csrf_token(url: str):
    """
    Метод поиска CSRF токена на странице

    :param url: URL сайта
    :type url: str
    :return: Удалось ли найти CSRF Token (0 - да, -1 - нет)
    :rtype: int
    """
    req = None
    name_token = ['csrf-token', 'csrfmiddlewaretoken', 'token', 'csrf']
    try:
        req = requests.get(url)
    except Exception:
        print("Error URl")
    soup = bs(req.text, 'lxml')
    try:
        print("CSRF token", soup.find('input', {'type': 'hidden', 'name': name_token})['value'])
    except TypeError:
        print("Not find CSRF token")
