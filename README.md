# Инструкция по использованию сканера
* [Начальные установки](#setup)
* [Xss-сканер](#xss)
* [SQL injection сканер](#sqli)
* [Поиск CSRF токена](#csrf)
* [Поиск полей ввода](#input)

## Начальные установки <a name="setup"></a>
Сканер написан на языке Python3. Создатели не несут ответсвенности за действия пользователей. Весь предоставленый функционал используется исключительно в учебных целях

    git clone https://gitlab.com/sim200347/web-scanner.git
    cd web-csanner
    pip install -r requirements

## XSS <a name="xss"></a>
Для сканирования XSS используйте следующую команду

    python3 main.py --xss start -u <your_website_url>

## SQL injection <a name="sqli"></a>
Для сканирования SQL injections используйте следующую команду

    python3 main.py --sqli start -u <your_website_url>
    
## CSRF token <a name="csrf"></a>
Для поиска CSRF токена используйте следующую команду

    python3 main.py --csrd start -u <your_website_url>
    
## Поиск полей ввода <a name="input"></a>
Для поиска CSRF токена используйте следующую команду

    python3 main.py --input start -u <your_website_url>