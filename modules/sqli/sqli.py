from modules.base_funcs.get_all_forms import get_all_forms
from modules.base_funcs.get_form_details import get_form_details
from modules.sqli.submit_form import submit_form


def scan_sqli(url):
    errors = {'SQLITE': 'sqlite3.OperationalError',
              'SQLite': 'SQLite3::query()',
              'MySQL': 'error in your SQL syntax',
              'MiscError': 'mysql_fetch',
              'MiscError2': 'num_rows',
              'Oracle': 'ORA-01756',
              'JDBC_CFM': 'Error Executing Database Query',
              'JDBC_CFM2': 'SQLServer JDBC Driver',
              'MSSQL_OLEdb': 'Microsoft OLE DB Provider for SQL Server',
              'MSSQL_Uqm': 'Unclosed quotation mark',
              'MS-Access_ODBC': 'ODBC Microsoft Access Driver',
              'MS-Access_JETdb': 'Microsoft JET Database',
              'Error Occurred While Processing Request': 'Error Occurred '
                                                         'While Processing '
                                                         'Request',
              'Server Error': 'Server Error',
              'Microsoft OLE DB Provider for ODBC Drivers error': 'Microsoft '
                                                                  'OLE DB '
                                                                  'Provider '
                                                                  'for ODBC '
                                                                  'Drivers '
                                                                  'error',
              'Invalid Querystring': 'Invalid Querystring',
              'OLE DB Provider for ODBC': 'OLE DB Provider for ODBC',
              'VBScript Runtime': 'VBScript Runtime',
              'ADODB.Field': 'ADODB.Field',
              'BOF or EOF': 'BOF or EOF',
              'ADODB.Command': 'ADODB.Command',
              'JET Database': 'JET Database',
              'mysql_fetch_array()': 'mysql_fetch_array()',
              'Syntax error': 'Syntax error',
              'mysql_numrows()': 'mysql_numrows()',
              'GetArray()': 'GetArray()',
              'FetchRow()': 'FetchRow()',
              'Input string was not in a correct format': 'Input string was '
                                                          'not in a correct '
                                                          'format',
              'Not found': 'Not found'}
    with open('payloads/sqli.txt') as file:
        content = file.readlines()
    forms = get_all_forms(url)
    flag = False
    payloads = [x.strip() for x in content]
    if not flag:
        for payload in payloads:
            if not flag:
                for form in forms:
                    form_details = get_form_details(form)
                    content = submit_form(form_details, url,
                                          payload).content.decode()
                    for db, error in errors.items():
                        if error in content:
                            print("\033[32m\033[1mSUCCESS\033[0m\n"
                                  '[+] Vulnerable\n['
                                  '!] ERROR : {}'
                                  '\n[!] DATABASE : {}\n[*] URL : {}\n [*] '
                                  'Payload: {}'.format(error, db, url, payload))
                            flag = True
                            break
                    if not flag:
                        print('\033[31m\033[1mERROR\033[0m\n'
                              '\033[31m\033[1m[!]\033[0m Vulnerability not '
                              'found. Check next payload {}'.format(payload))
            else:
                break
