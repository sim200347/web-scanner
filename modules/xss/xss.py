from pprint import pprint

from modules.base_funcs.get_all_forms import get_all_forms
from modules.base_funcs.get_form_details import get_form_details
from modules.xss.submit_form import submit_form


def scan_xss(url):
    """
    Given a `url`, it prints all XSS vulnerable forms and 
    returns True if any is vulnerable, False otherwise
    """
    # get all the forms from the URL
    forms = get_all_forms(url)
    print(f"[+] Detected {len(forms)} forms on {url}.")
    with open('payloads/xss.txt') as file:
        content = file.readlines()
    payloads = [x.strip() for x in content]
    flag = False
    count = 0
    for payload in payloads:
        count += 1
        js_script = payload
        # returning value
        is_vulnerable = False
        # iterate over all forms
        if not flag:
            for form in forms:
                form_details = get_form_details(form)
                content = submit_form(form_details, url,
                                      js_script).content.decode()
                if js_script in content:
                    print("\033[32m\033[1mSUCCESS\033[0m")
                    print(f"[+] XSS Detected on {url}")
                    print(f"[*] Form details:")
                    pprint(form_details)
                    inputs = form_details['inputs']
                    for input in inputs:
                        if input.get('value'):
                            print('Working payload {}'.format(
                                input.get('value')))
                            break
                    is_vulnerable = True
                    flag = True
                    # won't break because we want to print available vulnerable
                    # forms
            if not is_vulnerable:
                print("\033[31m\033[1m ERROR \033[0m")
                print(
                    '\033[31m\033[1m[!]\033[0mVulnerability are not detected. '
                    'Check next payload({})'.format(
                        count))
                print(f"[*] Form details:")
                pprint(form_details)
                print('------------------------------------')
    if not flag:
        print('It\'s secure site')
